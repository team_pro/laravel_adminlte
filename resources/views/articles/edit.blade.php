@extends('admin.admin_template')

@section('title','Editar Artículo - ' . $articles->title)

@section('content')
	@if(count($errors) > 0)
		<div class="alert alert-danger" role="alert">
			<ul>
				@foreach($errors->all() as $error)
					<li>{{ $error }}</li>
				@endforeach
			</ul>
		</div>
	@endif
	{!! Form::open(['route'=>['admin.articles.update', $articles->id],'method'=>'PUT']) !!}
		<div class="form-group">
			{!! Form::label('title','Título') !!}
			{!! Form::text('title',$articles->title,['class' => 'form-control','placeholder' => 'Título de Artículo', 'required', 'files' => true]) !!}
		</div>
		<div class="form-grpup">
			{!! Form::label('category_id','Categoria') !!}
			{!! Form::select('category_id', $categories, $articles->category->id,['class' => 'form-control select-category', 'placeholder' => 'Seleccione una Categoria' ,'required']) !!}
		</div>
		<div class="form-group">
			{!! Form::label('content', 'Contenido') !!}
			{!! Form::textarea('content', $articles->content, ['class' => 'form-control textarea-content']) !!}
		</div>
		<div class="form-group">
			{!! Form::label('tags', 'Tags')!!}
			{!! Form::select('tags[]',$tags, $my_tags,['class' => 'form-control select-tag','multiple','required'])!!}
		</div>
		<!-- <div class="form-group">
			{!! Form::label('image', 'Imagen') !!}
			{!! Form::file('image') !!}
		</div> -->
		<div class="form-group text-center">
			{!! Form::submit('Editar Artículo', ['class' => 'btn btn-primary']) !!}
		</div>
	{!! Form::close() !!}
@endsection
@section('js')
	<script>
		$('.select-tag').chosen({
			placeholder_text_multiple: 'Seleccione un máximo de 3 tags',
			max_selected_options : 3,
			no_results_text : 'No se encontró este Tag'
		});
		$('.select-category').chosen({
			no_results_text: 'No se encontró esta Categoria'
		});
		$('.textarea-content').trumbowyg();
	</script>
@endsection