@extends('admin.admin_template')
@section('title', 'Listado de Artículos')

@section('content')
	<a href="{{ route('admin.articles.create') }}" class="btn btn-info">Registrar nuevo Artículo</a>
	<!--Buscador de Artículos -->
		{!! Form::open(['route' => 'admin.articles.index', 'method' => 'GET', 'class' => 'navbar-form pull-right']) !!}
			<div class="input-group">
				{!! Form::text('title', null,['class' => 'form-control', 'placeholder' => 'Buscar Artículo', 'aria-describedby' => 'search']) !!}
				<span class="input-group-addon" id="search"><span class="glyphicon glyphicon-search"></span></span>
			</div>
		{!! Form::close() !!}
	<!-- Fin buscador -->
	<table class="table table-striped">
		<thead>
			<th>ID</th>
			<th>Título</th>
			<th>Categoria</th>
			<th>User</th>
			<th>Acción</th>
		</thead>
		<tbody>
			@foreach($articles as $article)
			<tr>
				<td>{{ $article->id }}</td>
				<td>{{ $article->title }}</td>
				<td>{{ $article->category->name }}</td>
				<td>{{ $article->user->name }}</td>
				<td>
					<a href="{{ route('admin.articles.edit', $article->id) }}" class="btn btn-warning glyphicon glyphicon-wrench"></a>
					<a href="{{ route('admin.articles.destroy', $article->id) }}" onclick="return confirm('¿Estas seguro de eliminar esta categoria?')" class="btn btn-danger glyphicon glyphicon-remove-circle"></a>
				</td>
			</tr>
			@endforeach
		</tbody>
	</table>
	<div class="text-center">
		{!! $articles->render() !!}
	</div>


@endsection