@extends('admin.admin_template')

@section('title', 'Listado de Categorias')
@section('content')
	<a href="{{ route('admin.categories.create') }}" class="btn btn-info">Registrar nueva categoria</a>
	<table class="table table-striped">
		<thead>
			<th>ID</th>
			<th>Name</th>
			<th>Action</th>
		</thead>
		@foreach($categories as $category)
		<tbody>
			<td>{{ $category->id }}</td>
			<td>{{ $category->name }}</td>
			<td>
				<a href="{{ route('admin.categories.edit', $category->id) }}" class="btn btn-warning glyphicon glyphicon-wrench" style="width: 15%; margin:3px "></a>
				<a href="{{ route('admin.categories.destroy', $category->id) }}" onclick="return confirm('¿Estas seguro de eliminar esta categoria?')" class="btn btn-danger glyphicon glyphicon-remove-circle" style="width: 15%; margin: 3px"></a>
			</td>
		</tbody>
		@endforeach
	</table>
	{!! $categories->render() !!}
@endsection