@extends('admin.admin_template')

@section('title', 'Modificar Categoria')
@section('content')

	{!! Form::open(['route' => ['admin.categories.update', $categories->id], 'method' => 'PUT']) !!}
		<div class="form-group">
			{!! Form::label('name', 'Nombre: ') !!}
			{!! Form::text('name',$categories->name,['class' => 'form-control']) !!}
		</div>
		<div class="form-group">
			{!! Form::submit('Editar',['class' => 'btn btn-primary']) !!}
		</div>
	{!! Form::close() !!}

@endsection