@extends('admin.admin_template')
@section('title','Listado de Tags')
@section('content')
	<a href="{{ route('admin.tags.create') }}" class="btn btn-info">Registrar nuevo Tag</a>
	<!--Buscador de Tags -->
		{!! Form::open(['route' => 'admin.tags.index', 'method' => 'GET', 'class' => 'navbar-form pull-right']) !!}
			<div class="input-group">
				{!! Form::text('name', null,['class' => 'form-control', 'placeholder' => 'Buscar Tag', 'aria-describedby' => 'search']) !!}
				<span class="input-group-addon" id="search"><span class="glyphicon glyphicon-search"></span></span>
			</div>
		{!! Form::close() !!}
	<!-- Fin buscador -->
	<table class="table table-striped">
		<thead>
			<th>ID</th>
			<th>Nombre</th>
			<th>Action</th>
		</thead>
		<tdoby>
			@foreach($tags as $tag)
			<tr>
				<td>{{ $tag->id }}</td>
				<td>{{ $tag->name }}</td>
				<td>
					<a href="{{ route('admin.tags.edit', $tag->id) }}" class="btn btn-warning glyphicon glyphicon-wrench" style="width: 15%; margin:3px "></a>
					<a href="{{ route('admin.tags.destroy', $tag->id) }}" onclick="return confirm('¿Estas seguro de eliminar esta categoria?')" class="btn btn-danger glyphicon glyphicon-remove-circle" style="width: 15%; margin: 3px"></a>
				</td>
			</tr>
			@endforeach
		</tdoby>
	</table>
	<div class="text-center">
		{!! $tags->render() !!}
	</div>
@endsection