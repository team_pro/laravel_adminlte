@extends('admin.admin_template')
@section('title', 'Editar Tag')
@section('content')
	{!! Form::open(['route' => ['admin.tags.update',$tags], 'method' => 'PUT']) !!}
		<div class="form-group">
			{!! Form::label('name', 'Nombre') !!}
			{!! Form::text('name',$tags->name,['class' => 'form-control', 'placeholder' => 'Nombre', 'required']) !!}	
		</div>
		<div class="form-group">
			{!! Form::submit('Registrar',['class' => 'btn btn-primary']) !!}
		</div>
	{!! Form::close() !!}
@endsection